//main() executes onLoad
function main() {
    const images = document.getElementsByTagName("img");
    for(let i = 0; i < images.length; i++) {
        images[i].ondragstart = function() { return false; }; //Copied to disable image dragging
    }
    cookieSetup();

    setInterval(cps, 1000);

    const upgradeArray = document.getElementsByClassName("upgradeIcon");
    for(let i = 0; i < upgradeArray.length; i++) {
        document.getElementById("upgradeCount" + i).innerHTML = parseInt(getCookie("upgrade" + i), 10);
        //TODO: add a event listener to each upgrade in upgradeArray instead of onclick in html file
        upgradeArray[i].addEventListener("click", function(){upgradeOnClick(i)});
    }
}
//Cookies
function cookieSetup() {
    scoreField.innerHTML = getCookie("score");

    const priceArray = document.getElementsByClassName("upgradePrice");
    for(let i = 0; i < priceArray.length; i++) {
        if(getCookie("price" + i) == 0) {
            setCookieInfinite("price" + i, getUpgradeDefautPrice(i));
        }
        priceArray[i].innerHTML = getCookie("price" + i);
    }
}

function getCookie(cookieName) {
    cookieName += "=";
    const decodedCookie = decodeURIComponent(document.cookie);
    const splitCookie = decodedCookie.split("; ");
    for(let i = 0; i < splitCookie.length; i++) {
        if(splitCookie[i].startsWith(cookieName)) {
            splitCookie[i] = splitCookie[i].replace(cookieName, "");
            return splitCookie[i];
        }
    }
    return 0;
}
function setCookie(cookieName, cookieValue, expiration) {
    const date = new Date();
    date.setTime(date.getTime() + expiration);
    const expirationDate = date.toUTCString();
    document.cookie = cookieName + "=" + cookieValue + ";expires=" + expirationDate + ";path=/";
}
function setCookieInfinite(cookieName, cookieValue) {
    setCookie(cookieName, cookieValue, Number.MAX_SAFE_INTEGER);
}

//Copied functions
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

//My functions
async function cps() { //TODO: Doesn't have to be async, at least I think so
    const cps = parseFloat(getCookie("cps"), 10);
    let score = parseFloat(getCookie("score"), 10);
    score += cps;
    setCookieInfinite("score", score);
    scoreField.innerHTML = getCookie("score");
}

function cookieOnClick() {
    let score = parseFloat(getCookie("score"), 10);
    const scoreMultiplier = parseFloat(getCookie("upgrade0"), 10) + 1;
    setCookieInfinite("score", score + scoreMultiplier);
    scoreField.innerHTML = getCookie("score");
}

function resetScore() {
    document.cookie.split(";").forEach(function(c) {
        document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/");
    }); //Copied for clearing all cookies
    const upgradeArray = document.getElementsByClassName("upgradeIcon");
    for(let i = 0; i < upgradeArray.length; i++) {
        document.getElementById("upgradeCount" + i).innerHTML = parseFloat(getCookie("upgrade" + i), 10);
    }
    scoreField.innerHTML = getCookie("score");
    document.getElementById("cpsField").innerHTML = "CPS: " + getCookie("cps");
    cookie.src = "assets/andrea.jpg";
    cookieSetup();
}

function happyCookie() {
    cookie.src = "assets/cookie_happy.png";
}

function upgradeOnClick(index) { //upgradeOnClick() executes when an upgrade is clicked. index is the index number of that upgrade
    const cpsMultiplierArray = [0, 1]; //how much will be cps value increased by (array index is also upgrade index)
    const upgradeName = "upgrade" + index;
    let upgradeValue = parseFloat(getCookie(upgradeName), 10);
    const price = getCookie("price" + index);
    const score = parseFloat(getCookie("score"), 10);
    if(price <= score) {
        upgradeValue++;
        setCookieInfinite(upgradeName, upgradeValue);
        document.getElementById("upgradeCount" + index).innerHTML = upgradeValue;
        
        setCookieInfinite("score", score - price);
        scoreField.innerHTML = getCookie("score");
        let cps = parseFloat(getCookie("cps"));
        cps += cpsMultiplierArray[index];
        setCookieInfinite("cps", cps);
        document.getElementById("cpsField").innerHTML = "CPS: " + getCookie("cps");
        upgradePrice(index);
    }
}

function upgradePrice(index) {
    let price = parseFloat(getCookie("price" + index), 10);
    let count = parseFloat(getCookie("upgrade" + index), 10);
    price += Math.pow(count, 1.1) * getUpgradeDefautPrice(index) / 2;
    price = Math.round(price);
    setCookieInfinite("price" + index, price);
    document.getElementById("upgradePrice" + index).innerHTML = price;
}

function getUpgradeDefautPrice(index) {
    const defaultPriceArray = [100, 250];
    return defaultPriceArray[index];
}

//TEST
function test() {
    score = parseFloat(getCookie("score"), 10);
    score += 1000;
    setCookieInfinite("score", score);
    scoreField.innerHTML = score;
}
